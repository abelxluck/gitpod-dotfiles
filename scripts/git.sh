#!/bin/bash
cd  /home/gitpod/.dotfiles/
gpg --import config/abel.key
gpgconf --kill all
git config --global alias.co "checkout"
git config --global alias.s "status -s"
git config --global alias.d "diff"
git config --global alias.ds "diff --staged"
git config --global alias.l 'log --pretty=format:"%C(yellow)%h\\ %ad%Cred%d\\ %Creset%s%Cblue\\ [%cn]" --decorate --date=short'
git config --global alias.ls 'log --pretty=format:"%C(yellow)%h%Cred%d\\ %Creset%s%Cblue\\ [%cn]" --decorate'
git config --global alias.ll 'log --pretty=format:"%C(yellow)%h%Cred%d\\ %Creset%s%Cblue\\ [%cn]" --decorate --numstat'
git config --global alias.gitree 'log --graph --oneline --decorate --all'
git config --global user.signingkey 884B649C340C81F4
git config --global pull.ff only # this is my preference
