#!/bin/bash

cd /home/gitpod/.dotfiles/
cp config/bashrc /home/gitpod/.bashrc.d/999-gp-opts-bashrc
cp config/fzf.bash /home/gitpod/.fzf.bash
